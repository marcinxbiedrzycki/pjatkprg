//Zadanie 2

#include <iostream>
#include <conio.h>
#include <math.h>

using namespace std;



int main()
{
	double a, b, r, delta;
	double x, x1, x2, y, y1, y2;

	cout << "Podaj a, b oraz r \n";
	cin >> a;
	cin >> b;
	cin >> r;
	delta = pow(2 * a*b, 2) - 4 * ((pow(a, 2) + 1)*(pow(b, 2) - pow(r, 2)));   // "pow" podnosi liczbe do potegi
	x = -b / 2 * a;
	x1 = -b - sqrt(delta) / 2 * a;
	x2 = -b + sqrt(delta) / 2 * a;
	y1 = a * x1 + b;
	y2 = a * x2 + b;
	y = a * x + b;


		if (delta > 0)
            {

			cout << "Prosta przecina okrag w dwoch punktach:";
			cout.setf(ios::fixed);
			cout.setf(ios::showpoint);
			cout.width(8);
			cout.precision(3);

			cout << "x1= " << x1;

			cout.setf(ios::fixed);
			cout.setf(ios::showpoint);
			cout.width(8);
			cout.precision(3);
			cout << "x2= " << x2 << endl;

			cout.setf(ios::fixed);
			cout.setf(ios::showpoint);
			cout.width(8);
			cout.precision(3);
			cout << "y1= " << y1;

			cout.setf(ios::fixed);
			cout.setf(ios::showpoint);
			cout.width(8);
			cout.precision(3);
			cout << "y2= " << y2 << endl;


		}
		else if (delta == 0)
            {
			cout << "Prosta jest styczna do okregu w punktach:";
			cout.setf(ios::fixed);
			cout.setf(ios::showpoint);
			cout.width(8);
			cout.precision(3);
			cout << "x= " << x;

			cout.setf(ios::fixed);
			cout.setf(ios::showpoint);
			cout.width(8);
			cout.precision(3);
			cout << "y= " << y;

		}
		else
            {
			cout << "Brak punktow wspolnych z okregiem";
		}


		_getch();
		return 0;
}
