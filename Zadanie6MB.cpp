
// Zadanie 6

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;
int podajLiczbe();

int main() {
	int a = podajLiczbe();
	cout << "\n";
	cout << "Dzielniki liczby "<<endl;    // Obliczenie wspolnego dzielnika z pomoca petli for
	for (int i = 1; i <= a; i++) {
		if (a % i == 0) {
			cout << i << '\n';
		}
	}
		/*
		cout << "Dzielniki liczby"<<endl;              //Obliczenie wspolnego dzielnika z uzyciem petli while
		int j = 1;
		while (j <= a) {
			if (a % j == 0) {
				cout << j << '\n';
			}
			j++;
		}
		cout << "Dzielniki liczby "<<endl;              //Obliczenie wspolnego dzielnika z uzyciem petli do while
		int k = 1;
		do {
			if (a % k == 0) {
				cout << k << '\n';
			}
			k++;
		} while (k <= a);
		*/
	_getch();
	return 0;
}

int podajLiczbe() {
	int n;
	cout << "Podaj liczbe" << endl;
	cin >> n;
	if ( n <= 0) {
		cin.clear();
		cout << "Niepoprawna wartosc." << endl;
		cout <<"Podaj liczbe" << endl;
		cin >> n;
	}
	return n;
}
