import javax.swing.JOptionPane;


public abstract class Winda implements Pomieszczenie {

	int maxIloscOsob;
	int iloscOsob;
	String nazwa;
	int aktualnePietro;
	
	public Winda(String nazwa)
	{
		this.iloscOsob = 0;
		this.aktualnePietro = 0;
		this.nazwa = nazwa;
	}
	
	
	public Winda()
	{
		this.iloscOsob = 0;
	}
	
	public void PokazStatus()
	{
		System.out.println("Osoby w " + this.nazwa + " : " + this.iloscOsob + "/" + this.maxIloscOsob + ". Jest na pietrze nr " + this.aktualnePietro);
	}
	
	public void Wpusc(int osoby) throws WindaException
	{
		OtworzDrzwi();
		if (this.iloscOsob + osoby <= maxIloscOsob)
		{
			this.iloscOsob += osoby;
		}
		else 
		{
			throw new WindaException("Za du�o, przeci��enie");
		}
			//JOptionPane.showMessageDialog(null, "Za du�o, przeci��enie");	
		ZamknijDrzwi();
	}
	
	public void ZamknijDrzwi()
	{
		System.out.println("Drzwi zamknieto");
	}
	
	public void OtworzDrzwi()
	{
		System.out.println("Drzwi otwarto");
	}
	
	public void Wypusc(int osoby) throws WindaException
	{
		OtworzDrzwi();
		if (this.iloscOsob - osoby <0 )
		{
			throw new WindaException("Wychodzi wiecej osob niz obecnie jest w windzie?");
		}
		else
		{
			this.iloscOsob -= osoby;
		}
		
		ZamknijDrzwi();
	}
	
	public void JedzGora()
	{
		this.aktualnePietro ++;
	}
	
	public void JedzDol()
	{
		this.aktualnePietro --;
	}

	public abstract void WlaczAlarm();


	@Override
	public void Oswietl() {
		System.out.println("Uzyj dowolnej zarowki");
		
	}
}
	