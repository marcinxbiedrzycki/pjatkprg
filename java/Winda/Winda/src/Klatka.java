import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;


public class Klatka {

	public static List<Pietro> pietra;
	  										// pola z Windami i lista pi�ter jako sk�ad klatki
	public static Mitshubishi winda1;
	public static Thyssenkrup winda2;
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		pietra = new ArrayList<Pietro>();
		ZamowWindy();
											// U�ycie interfejsu, zapalenie klatki
		winda1.Oswietl();
		
		System.out.println("WItamy na klatce przy ul.Focha 12");
		PokazStatusWind();
		
		//////////////AKCJA
		
		//winda1.WlaczAlarm();
		
		try {
			winda1.Wpusc(2);
			winda1.JedzGora();
		} catch (WindaException e1) {
			// TODO Auto-generated catch block						// Wyj�tek, w przypadku zbyt du�ej ilo��i os�b w kabinie windy
			e1.printStackTrace();
			JOptionPane.showMessageDialog(null, "Za du�o, przeci��enie");	
		}
		
		PokazStatusWind();				// Ilosc os�b w danej windzie
		
		try {
			
			winda1.Wpusc(3);
			winda1.Wypusc(1);
			
			winda2.JedzGora();
		} catch (WindaException e) {
			
			//e.printStackTrace();
			JOptionPane.showMessageDialog(null, "Za du�o w windzie, przeci��enie");	
		}
		try {
			winda2.Wpusc(5);
			winda2.JedzGora();
		}catch (WindaException e) {
			JOptionPane.showMessageDialog(null, "Za du�o os�b w windzie, przeci��enie");
		}
		
		try {
			winda2.Wypusc(10);
		}catch(WindaException e) {
			JOptionPane.showMessageDialog(null, "Nie ma tylu os�b w windzie");
		}
		
		
		PokazStatusWind();
		
		//////////////
		
	}
	
	// Poni�sze funkcje s� interesuj�ce dla administratora klatki
	
	private static void PokazStatusWind()
	{																// status obu wind
		winda1.PokazStatus();
		winda2.PokazStatus();
	}
	
	private static void ZamowWindy()
	{
		winda1 = new Mitshubishi(6);
		winda2 = new Thyssenkrup (8);
	}

}
