#include <iostream>
#include <conio.h>
#include <Windows.h>
using namespace std;

void gotoxy(int x, int y);
int wherex();
int wherey();
void clreol();

const int Esq = 27;

int main()
{
	int x, y, x1, y1, size, i, j, q, w;
	char klawisz, symbol;
	x = 40; y = 10; size = 0;    // wspolrzedne, w ktorych zostaje rysowana figura
	HANDLE uchwyt;
	uchwyt = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(uchwyt, 10);
	cout << "Aby powiekszyc lub pomniejszyc figure, korzystaj z przyciwskow + i -" << endl;
	cout << "Aby zmienic polozenie figury korzystaj z strzalek " << endl;
	cout << "Podaj wielkosc figury " << endl;
	cin >> size;
	while (size>13)
		{
		cout << "Zostala wprowadzona zla wartosc wielkosci figury, podaj wartosc jeszcze raz" << endl;
			cin >> size;

		}
	cout << "Podaj znak jakim ma zostac narysowana figura " << endl;
	cin >> symbol;
		
	do
		{
			system("cls");
			x1 = x; y1 = y;                              // punkt zaczepienia
			gotoxy(x1, y1);
				for (i = 0; i<size; i++)				// rysowanie gornego odcinka figury
					
				{
						for (j = 0; j<size; j++)     
				
						{
							if (i == j){

								gotoxy(x + i, y + j);
							cout << symbol;
										}
						}
				}
		
				for (q = 0; q<size; q++)
		{
			for (w = 0; w<size; w++)				// rysowanie dolnego odcinka figury
			{

				if ((q + w) == size)
				{
					gotoxy(x + q - 1, size + y + w - 1);
					cout << symbol;
				}

			}
		}	
		klawisz = _getch();
		switch (klawisz)
		{
		case '+': {				// powiekszanie figury
					  {
						  if ((y<25 - (2 * size)) && (x + size<79))  
						  size = size + 1;

						  break; }}
		case '-': {				// pomniejszanie figury
					  if (size>2)
						  size = size - 1;
					  break; }
		case 75: {				// przesuniecie figury w lewo
					 if (x>0) x--;

					 break; }
		case 77: {				// przesuni�cie figury w prawo
					 if (x + size<79) x++;
					 break; }
		case 72: {				// przesuni�cie figury w g�r�
					 if (y>0)	y--;
					 break; }
		case 80: {				// przesuniecie figury w d�
					 if (y<25 - (2 * size))	 y++;
					 break; }
		case Esq: { break; }
		}

	} while (klawisz != Esq);  // wyj�cie z programu za pomoca esc
	return 0;
}
//----------------------------------------------------------------------------
void gotoxy(int x, int y)
{
	COORD c;
	c.X = x;
	c.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
}
//----------------------------------------------------------------------------
int wherex()
{

	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	return csbi.dwCursorPosition.X;
}
//----------------------------------------------------------------------------
int wherey()
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &csbi);
	return csbi.dwCursorPosition.Y;
}
//----------------------------------------------------------------------------
void clreol()
{
	int x, y;
	x = wherex();
	y = wherey();
	gotoxy(x, y);
	for (int i = x; i <= 79; i++)
		cout << " ";
	gotoxy(x, y);
}
