

#include <iostream>
#include <conio.h>
#include <math.h>
#define _USE_MATH_DEFINES

using namespace std; 

int main() {
	double r;
	double stopnie;
	double radiany;
	double f; 
	
	cout << "Podaj promien kola i kat rozwarcia:";
	cin >> r;
	cin >> stopnie;
	radiany = M_PI * stopnie / 180;
	f = (r * r)*(radiany - sin(stopnie))/2 ;
	cout << "Pole: " << f;
	_getch();
	return 0;
}
